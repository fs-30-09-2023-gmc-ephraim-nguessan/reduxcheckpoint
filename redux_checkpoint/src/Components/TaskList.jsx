import React from 'react';

const TaskList = ({ tasks, onToggleDone, onDelete }) => {
  return (
    <ul>
      {tasks.map(task => (
        <li key={task.id}>
          <input
            type="checkbox"
            checked={task.isDone}
            onChange={() => onToggleDone(task.id)}
          />
          <span style={{ textDecoration: task.isDone ? 'line-through' : 'none' }}>
            {task.description}
          </span>
          <button onClick={() => onDelete(task.id)}>Supprimer</button>
        </li>
      ))}
    </ul>
  );
}

export default TaskList;
