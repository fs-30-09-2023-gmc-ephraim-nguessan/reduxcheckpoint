import React from 'react';

const Task = ({ task, onToggleDone, onDelete }) => {
  return (
    <li>
      <input
        type="checkbox"
        checked={task.isDone}
        onChange={() => onToggleDone(task.id)}
      />
      <span style={{ textDecoration: task.isDone ? 'line-through' : 'none' }}>
        {task.description}
      </span>
      <button onClick={() => onDelete(task.id)}>Supprimer</button>
    </li>
  );
}

export default Task;
