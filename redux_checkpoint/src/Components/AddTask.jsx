import React, { Component } from 'react';

class AddTask extends Component {
  state = {
    description: ''
  };

  handleChange = (e) => {
    this.setState({ description: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.description.trim()) {
      this.props.onAddTask({ description: this.state.description, isDone: false });
      this.setState({ description: '' });
    }
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Ajouter une nouvelle tâche"
            value={this.state.description}
            onChange={this.handleChange}
          />
          <button type="submit">Ajouter</button>
        </form>
      </div>
    );
  }
}

export default AddTask;
